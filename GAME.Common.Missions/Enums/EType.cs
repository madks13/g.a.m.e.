﻿using System;

namespace GAME.Common.Missions.Enums
{
    public enum Type
    {
        None,
        Alert,
        Invasion,
        Outbreak
    }
}
