﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GAME.Modules.Warframe.AlertScanner.Interfaces.Reward
{
    interface IReward
    {
        String Text { get; set; }
    }
}
