# README #

### G.A.M.E. ###

* Game Activity Managed Electronically is a program aiming to provide numerous small modules that help gamers manage their game activity in various ways.
* 0.9.1

### Warframe ###
* Warframe utility that help players organize their game activity
* Features
    * **Alert scanner : scans the Warframe RSS feed to check on available alerts. If new are found, it alerts the player.**

### How do I get set up? ###

* Summary of set up
    * **Download/Clone repo**
    * **Open in Visual Studio 2010 or later**
* Configuration
    * **Windows 7 or later**
    * **.NET 4.5.1**
* Dependencies
    * **.NET 4.5.1**
    * **Visual Studio 2010+**
* Database configuration
    * **None yet**
* How to run tests
    * **Run the unit testing**
    * **Compile, launch**
* Deployment instructions

### Contribution guidelines ###

* To be added soon
* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* nikola.tomasevic@epitech.eu